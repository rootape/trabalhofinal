import leafPage
import bPTree

class interPage:
    #n e o tamanho da pagina
    n = 0
    
    #kPonts e o par Keys - Ponteiros
    kPonts = [[float("inf"), None]]
    
    #dadPont aponta para a pagina acima
    dadPont = None
    
    #tree e uma referencia a arvore da pagina
    tree = None

    def __init__(self, tree, num):
        self.n = num
        self.tree = tree

    #metodo que modifica um ponteiro dado uma pagina
    #ele procura nos kPonts ate achar um par que possua a page
    #depois substitui essa page por uma nova
    def setNewPont(self, page, ppage):
        for i in range(0, len(self.kPonts)):
            if page is (self.kPonts[i])[1]:
                break
        (self.kPonts[i])[1] = ppage

    #metodo que indica quem e a pagina pai
    def setDad(self, page):
        self.dadPont = page

    #insere um kPont e ordena a lista
    def cmInsert(self, x):
        (self.kPonts).append(x)
        (self.kPonts).sort()

    #metodo de insercao principal
    def insert(self, x):

        print "Tamanho no Inter: "
        print self
        print len(self.kPonts)
        save = 0
        
        #procura na pagina qual o lugar que x deve ser inserido
        for i in range(0, len(self.kPonts) - 1):
            if x[0] > (self.kPonts[i])[0]:
                save = i + 1

        #chama um metodo recursivamente, recebendo como retorno um par [Boolean, valor]
        resLst = (self.kPonts[save])[1].insert(x)

        #caso o boolean retorne True, ou seja, caso na pagina abaixo tenha havido um split
        #e a pagina nao seja a raiz da arvore, entra no if
        if(resLst[0] and not(self is (self.tree).getRoot())):
            
            #faz uma insercao comum do valor retornado
            self.cmInsert(resLst[1])
            
            #caso seja necessario fazer um split, entra no if
            if(len(kPonts) - 1) == self.n:

                print "Splitei na Inter e n sou raiz"
                
                #pega o meio das chaves
                half = int((len(self.kPonts)-1)/2)

                #cria duas paginas novas pro split
                p1 = (self.tree).createIPage()

                #pega da primeira chave ate antes da metade
                #e salva na primeira pagina
                for i in range(0, half):
                    p1.cmInsert(self.kPonts[i])
                    (self.kPonts).pop(i)

                p2 = (self.tree).createIPage()
                p1.setDad(self.dadPont)
                p2.setDad(self.dadPont)
                    
                #pega da pagina seguinte a metade ate a
                #ultima CHAVE e salva na segunda pagina
                for i in range(half+1, len(self.kPonts) - 1):
                    p2.cmInsert(self.kPonts[i])
                    (self.kPonts).pop(i)
                    
                #pega a pagina do ultimo ponteiro DESSA pagina e
                #salva no ultimo ponteiro de p2
                p2.setNewPont([float("inf"), None], (self.kPonts[1])[1])
                
                #pega a pagina da chave que subiu no split e coloca no
                #ultimo ponteiro de p1
                p1.setNewPont([float("inf"), None], (self.kPonts[0])[1])

                #muda o ponteiro da chave que vai subir para p1 
                (self.kPonts[0])[1] = p1
                
                #muda o ponteiro da chave que apontava para essa
                #pagina para p2
                (self.dadPont).setNewPont(self, p2)

                #retorna True porque houve o split e o par kPont para
                #ser inserido na pagina pai
                return [True, self.kPonts[0]]
            
            #caso nao entre no if, retorna False
            return [False, None]

        #caso a pagina tenha que inserir algo e e a raiz da arvore
        #entra no if
        elif(resLst[0] and self is (self.tree).getRoot()):
            #insere o que subiu
            self.cmInsert(resLst[1])

            #se precisar fazer o split
            if(len(self.kPonts) - 1) == self.n:

                print "Splitei na Inter e sou raiz"

                #cria 3 paginas, uma que sera a nova raiz
                #e suas filhas
                half = int((len(self.kPonts)-1)/2)
                p1 = (self.tree).createIPage()

                #insere de 0 ate a metade em p1
                for i in range(0, half):
                    p1.cmInsert(self.kPonts[i])
                    (self.kPonts).pop(i)

		p2 = (self.tree).createIPage()

                #inserde do sucessor da metade ate o fim em p2
                for i in range(half+1, len(self.kPonts) - 1):
                    p2.cmInsert(self.kPonts[i])
                    (self.kPonts).pop(i)

		p3 = (self.tree).createIPage()

		p1.setDad(p3)
                p2.setDad(p3)

                #coloca o ultimo ponteiro dessa pagina no ultimo de p2
                p2.setNewPont([float("inf"), None], (self.kPonts[1])[1])

                #coloca o ponteiro do valor que vai subir no ultimo de p1
                p1.setNewPont([float("inf"), None], (self.kPonts[0])[1])

                #coloca p2 como o ultimo ponteiro de p3
                p3.setNewPont([float("inf"), None], p2)

                #coloca o valor do ponteiro do par que vai subir como p1
                (self.kPonts[0])[1] = p1

                #insere o valor que vai subir em p3
                p3.cmInsert(self.kPonts[0])

                #coloca p3 como a nova raiz
                (self.tree).setRoot(p3)

                return [False, None]
                
        #caso nao precise fazer nada, retorna False
        return [False, None]

    def search(self, x):
        save = 0

        #itera pelos kPonts, buscando em qual ponteiro deve seguir
        for i in range(0, len(self.kPonts)-1):
            if x > (self.kPonts[i])[0]:
                save = i+1

        #retorna entao o ponteiro que deve ser seguido
        return ((self.kPonts[save])[1]).search(x)
