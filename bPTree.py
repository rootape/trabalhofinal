import interPage
import leafPage

class bPTree:
    #n sera o tamanho das paginas da arvore
    n = 0
    #root sera a raiz da arvore
    root = None

    def __init__(self, num):
        self.n = num
        self.setRoot(leafPage.leafPage(self, self.n))

    def insert(self, key, value):
        group = [key, value]
        (self.root).insert(group)

    def getRoot(self):
        return self.root

    def setRoot(self, page):
        self.root = page

    def search(self, x):
        print (self.root).search(x)

    def createLPage(self):
        return leafPage.leafPage(self, self.n)

    def createIPage(self):
        return interPage.interPage(self, self.n)
        

    
